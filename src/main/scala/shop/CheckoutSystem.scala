package shop

import shop.products.{Apple, Orange, ProductFactory}

/**
  * Created by sebastian on 16.05.17.
  */
object CheckoutSystem {
  def checkout(productNames: Seq[String]): Double = {
    productNames.map(
      productName => ProductFactory(productName).get
    )
      .groupBy(product => product)
      .map {
        case (Apple, xs) => adjustProductsPrice(xs.size, 2, 1, Apple.price)
        case (Orange, xs) => adjustProductsPrice(xs.size, 3, 2, Orange.price)
        case _ => throw new Error("Unsupported product")
      }
      .sum
  }

  private def adjustProductsPrice(productsCount: Int,
                                  minNumberOfProducts: Int,
                                  chargedProducts: Int,
                                  productPrice: Double): Double = {
    (productsCount % minNumberOfProducts * productPrice) + (productsCount / minNumberOfProducts * chargedProducts * productPrice)
  }

  def main(args: Array[String]) {
    println(checkout(args.toSeq))
  }
}
