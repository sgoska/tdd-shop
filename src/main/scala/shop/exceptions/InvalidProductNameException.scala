package shop.exceptions

/**
  * Created by sebastian on 16.05.17.
  */
class InvalidProductNameException(message: String) extends Exception(message)