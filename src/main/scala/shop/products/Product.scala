package shop.products

/**
  * Created by sebastian on 16.05.17.
  */
trait Product {
  def price: Double
}
