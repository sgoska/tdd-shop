package shop.products

/**
  * Created by sebastian on 16.05.17.
  */
case object Orange extends Product {
  val Name = "Orange"

  override def price: Double = 0.25
}
