package shop.products

import shop.exceptions.InvalidProductNameException

import scala.util.Try

/**
  * Created by sebastian on 16.05.17.
  */
object ProductFactory {
  def apply(productName: String): Try[Product] = {
    Try {
      productName match {
        case Apple.Name => Apple
        case Orange.Name => Orange
        case _ => throw new InvalidProductNameException(s"Invalid product name: $productName")
      }
    }
  }
}
