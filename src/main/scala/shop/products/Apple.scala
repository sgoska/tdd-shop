package shop.products

/**
  * Created by sebastian on 16.05.17.
  */
case object Apple extends Product {
  val Name = "Apple"

  override def price: Double = 0.6
}
