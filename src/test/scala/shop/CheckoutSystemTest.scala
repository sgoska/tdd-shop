package shop

import org.scalatest.{FlatSpec, Matchers}
import shop.products.{Apple, Orange}

/**
  * Created by sebastian on 16.05.17.
  */
class CheckoutSystemTest extends FlatSpec with Matchers {
  it should "return 0 for empty collection of products" in {
    CheckoutSystem.checkout(Seq()) shouldEqual 0.0
  }

  it should "return product price for one product" in {
    CheckoutSystem.checkout(Seq(Apple.Name)) shouldEqual Apple.price
  }

  it should "return sum of product prices" in {
    CheckoutSystem.checkout(
      Seq(Apple.Name, Orange.Name)
    ) shouldEqual Apple.price + Orange.price
  }

  it should "return price of one apple if two apples are bought" in {
    CheckoutSystem.checkout(
      Seq(Apple.Name, Apple.Name)
    ) shouldEqual Apple.price
  }

  it should "return price of two oranges if three oranges are bought" in {
    CheckoutSystem.checkout(
      Seq(Orange.Name, Orange.Name, Orange.Name)
    ) shouldEqual 2 * Orange.price
  }

  it should "return price of four oranges plus price of two apples if six oranges and four apples are bought" in {
    val oranges = (0 until 6).map(_ => Orange.Name)
    val apples = (0 until 4).map(_ => Apple.Name)
    CheckoutSystem.checkout(
      oranges union apples
    ) shouldEqual 4 * Orange.price + 2 * Apple.price
  }
}
