package shop.products

import org.scalatest.FlatSpec
import org.scalatest._
import shop.exceptions.InvalidProductNameException

/**
  * Created by sebastian on 16.05.17.
  */
class ProductFactoryTest extends FlatSpec with Matchers {
  it should "produce apple" in {
    ProductFactory(Apple.Name).get should be theSameInstanceAs Apple
  }

  it should "produce orange" in {
    ProductFactory(Orange.Name).get should be theSameInstanceAs Orange
  }

  it should "throw exception if product name is invalid" in {
    a[InvalidProductNameException] should be thrownBy {
      ProductFactory("InvalidProductName").get
    }
  }
}
